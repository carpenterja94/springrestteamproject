package com.allstate.spring.services;

import com.allstate.spring.entities.Customer;
import com.allstate.spring.entities.Interaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface InteractionService {
    Collection<Interaction> findAll();

    Interaction getInteractionById(int id);

    Interaction addInteraction(Interaction interaction);

    Page<Interaction> findInteractionByCustomerId(int customerId, Pageable pageable);

    Interaction updateInteraction(Interaction updateInteraction);

    void delete(int id);

}
