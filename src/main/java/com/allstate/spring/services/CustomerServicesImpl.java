package com.allstate.spring.services;


import com.allstate.spring.entities.Customer;
import com.allstate.spring.entities.Interaction;
import com.allstate.spring.repo.CustomerRepository;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.lang.invoke.MethodType;
import java.util.Collection;
import java.util.List;

@Service
public class CustomerServicesImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Iterable<Customer> findAllFilter(boolean isDeleted) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedCustomerFilter");
        filter.setParameter("isDeleted", isDeleted);
        Iterable<Customer> customers =  customerRepository.findAll();
        session.disableFilter("deletedCustomerFilter");
        return customers;
    }

    @Override
    public Collection<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.findById(id).get();
    }

    @Override
    public Customer updateCustomer(Customer updateCustomer) {
        return customerRepository.save(updateCustomer);
    }

    @Override
    public void delete(int id) {
        customerRepository.deleteById(id);
    }

    @Override
    public Customer addCustomer(Customer customer) {
        customer.setId(0); // assume it is not in the db
        return customerRepository.save(customer);
    }


}
