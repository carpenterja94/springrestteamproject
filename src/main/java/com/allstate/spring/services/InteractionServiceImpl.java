package com.allstate.spring.services;

import com.allstate.spring.entities.Customer;
import com.allstate.spring.entities.Interaction;
import com.allstate.spring.repo.InteractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Collection;


@Service
public class InteractionServiceImpl implements InteractionService{
    @Autowired
    private InteractionRepository interactionRepository;

    @Override
    public Collection<Interaction> findAll() {
        return interactionRepository.findAll();
    }

    @Override
    public Interaction getInteractionById(int id) {
        return interactionRepository.findById(id).get();
    }

    @Override
    public Interaction addInteraction(Interaction interaction) {
        interaction.setId(0);
        return interactionRepository.save(interaction);
    }

    @Override
    public Page<Interaction> findInteractionByCustomerId(int customerId, Pageable pageable) {
        return interactionRepository.findInteractionByCustomerId(customerId, pageable);
    }

    @Override
    public Interaction updateInteraction(Interaction updateInteraction) {
        return interactionRepository.save(updateInteraction);
    }

    @Override
    public void delete(int id) {
        interactionRepository.deleteById(id);
    }
}
