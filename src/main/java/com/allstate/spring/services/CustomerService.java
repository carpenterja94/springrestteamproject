package com.allstate.spring.services;

import com.allstate.spring.entities.Customer;
import com.allstate.spring.entities.Interaction;

import java.util.Collection;
import java.util.List;

public interface CustomerService {
    Iterable<Customer> findAllFilter(boolean deleted);

    Collection<Customer> findAll();

    Customer getCustomerById(int id);

    Customer updateCustomer(Customer updateCustomer);

    Customer addCustomer(Customer customer);

    void delete(int id);
}
