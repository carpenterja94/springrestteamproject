package com.allstate.spring.repo;

import org.springframework.data.domain.Page;
import com.allstate.spring.entities.Interaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;



@Repository
public interface InteractionRepository extends JpaRepository<Interaction, Integer> {
    Page<Interaction> findInteractionByCustomerId(int customerId, Pageable pageable);
}

