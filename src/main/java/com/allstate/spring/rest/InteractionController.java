package com.allstate.spring.rest;

import com.allstate.spring.entities.Customer;
import com.allstate.spring.entities.Interaction;
import com.allstate.spring.services.CustomerService;
import com.allstate.spring.services.InteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/interactions")
@CrossOrigin
public class InteractionController {
    @Autowired
    private InteractionService interactionService;

    @GetMapping
    public Collection<Interaction> getInteractions() {
        return interactionService.findAll();
    }

    @GetMapping("/{interactionId}")
    public Interaction getInteractionById(@PathVariable("interactionId") int id) {
        return interactionService.getInteractionById(id);
    }

    @PostMapping
    public void addInteraction(@RequestBody Interaction interaction) {
        interactionService.addInteraction(interaction);
    }

    @GetMapping("/customer/{customerId}")
    public Page<Interaction> getAllCommentsByPostId(@PathVariable (value = "customerId") int customerId,
                                                    Pageable pageable) {
        return interactionService.findInteractionByCustomerId(customerId, pageable);
    }

    @PutMapping
    public Interaction updateInteraction(@RequestBody Interaction updateInteraction) {
        return interactionService.updateInteraction(updateInteraction);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable int id) {
        interactionService.delete(id);
    }



}
