package com.allstate.spring.rest;

import com.allstate.spring.entities.Customer;
import com.allstate.spring.entities.Interaction;
import com.allstate.spring.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/customers")
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public Collection<Customer> getCustomers() {
        return customerService.findAll();
    }

    @GetMapping("/notdeleted")
    //@RequestMapping(method= RequestMethod.GET)
    public Iterable<Customer> getCustomersFiltered(@RequestParam(value = "deleted", required = false, defaultValue = "false") boolean deleted)
    {
        return customerService.findAllFilter(deleted);
    }

    @GetMapping("/{customerId}")
    public Customer getCustomerById(@PathVariable("customerId") int id) {
        return customerService.getCustomerById(id);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable int id) {
        customerService.delete(id);
    }

    @PutMapping
    public Customer updateCustomer(@RequestBody Customer updatedCustomer) {
        return customerService.updateCustomer(updatedCustomer);
    }

    @PostMapping
    public void addCustomer(@RequestBody Customer customer) {
        customerService.addCustomer(customer);
    }

//    @GetMapping("/records/{customerId}")
//    public List<Interaction> getCustomerRecordsById(@PathVariable("customerId") int id) {
//        return customerService.getCustomerRecordsById(id);
//    }
}
