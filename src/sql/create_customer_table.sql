CREATE TABLE `conygre`.`customer` (
                                      `id` INT NOT NULL AUTO_INCREMENT,
                                      `first_name` VARCHAR(45) NOT NULL,
                                      `last_name` VARCHAR(45) NOT NULL,
                                      `city` VARCHAR(45) NOT NULL,
                                      `zip_code` INT NOT NULL,
                                      `date_joined` VARCHAR(45) NOT NULL,
                                      PRIMARY KEY (`id`));

use conygre;
insert into customer values (1, 'Xizhen', 'Yang', 'LA', 90275, '7/9/2021');
