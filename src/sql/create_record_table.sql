CREATE TABLE record (id int primary key auto_increment,
                     customer_id int not null,
                     medium  varchar(50) not null,
                     comment varchar(500) ,
                     resolution_status boolean DEFAULT false not null,
                     record_date varchar(50),

                     FOREIGN KEY (customer_id) REFERENCES customer(id)

);


insert into record values(1,1,'phone','ask for contact',true,'7/4/2021');
insert into record values(2,1,'email','change username',true,'7/8/2021');
insert into record values(3,2,'website','item return',false,'6/13/2020');
insert into record values(4,3,'instagram','change profile',true,'7/5/2021');